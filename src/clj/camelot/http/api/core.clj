(ns camelot.http.api.core
  (:require
   [compojure.api.sweet :refer [api context]]
   [camelot.http.api.camera.core :as camera]
   [camelot.http.api.survey.core :as survey]
   [camelot.http.api.site.core :as site]
   [camelot.http.api.trap-station.core :as trap-station]
   [camelot.http.api.trap-station-session.core :as trap-station-session]
   [camelot.http.api.trap-station-session-camera.core :as trap-station-session-camera]
   [clojure.tools.logging :as log]
   [ring.util.http-response :as hr]
   [camelot.http.api.dataset.core :as dataset]))

(def routes
  (context "/api/v1" []
    :coercion :spec
    dataset/routes
    camera/routes
    survey/routes
    site/routes
    trap-station/routes
    trap-station-session/routes
    trap-station-session-camera/routes))

(defn- exception-handler
  [e _ _]
  (log/error "Exception while processing request:" e)
  (hr/internal-server-error {:type "unknown-exception" :class (.getName (.getClass e))}))

(def core-api
  (api
   {:exceptions {:handlers {:compojure.api.exception/default exception-handler}}
    :swagger
    {:ui "/api"
     :spec "/api/v1/swagger.json"
     :data {:info {:title "Camelot API"
                   :description "RESTful API for Camelot"}}
     :consumes ["application/json"]
     :produces ["application/json"]}}
   routes))
