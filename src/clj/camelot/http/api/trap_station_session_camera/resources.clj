(ns camelot.http.api.trap-station-session-camera.resources
  (:require
   [camelot.http.api.trap-station-session-camera.spec :as spec]
   [camelot.model.camera :as camera]
   [camelot.model.trap-station-session :as trap-station-session]
   [camelot.model.trap-station-session-camera :as trap-station-session-camera]
   [cats.core :as m]
   [cats.monad.either :as either]
   [clojure.edn :as edn]
   [ring.util.http-response :as hr]
   [camelot.http.api.util :as api-util]))

(def resource-type :trap-station-session-camera)
(def resource-base-uri "/api/v1/trap-station-session-cameras")

(defn- hoist
  [v f lv]
  (if v
    (either/right (f v))
    (either/left lv)))

(defn- map-not-found
  [f v]
  (hoist v f {:error/type :error.type/not-found}))

(defn validate-patch-camera
  [state patch]
  (if (:error/type patch)
    patch
    (if-let [tssid (:camera-id patch)]
      (hoist (camera/get-specific state tssid)
             (constantly patch)
             {:error/type :error.type/bad-request})
      (either/right patch))))

(defn- validate-patch-trap-station-session
  [state patch]
  (if (:error/type patch)
    patch
    (if-let [tssid (:trap-station-session-id patch)]
      (hoist (trap-station-session/get-specific state tssid)
             (constantly patch)
             {:error/type :error.type/bad-request})
      (either/right patch))))

(defn- validate-patch
  [state patch]
  (->> patch
       (validate-patch-trap-station-session state)
       (validate-patch-camera state)))

(defn- validate-post
  [state patch]
  (if-let [tssid (:trap-station-session-id patch)]
    (if-let [camid (:camera-id patch)]
      (hoist (and (trap-station-session/get-specific state tssid)
                  (camera/get-specific state camid))
             (constantly patch)
             {:error/type :error.type/not-found})
      (either/left {:error/type :error.type/bad-request}))
    (either/left {:error/type :error.type/bad-request})))

(defn patch [state id data]
  (let [mr (m/->>=
            (either/right data)
            (api-util/transform-request resource-type :camelot.http.api.trap-station-session-camera.patch/attributes id)
            (validate-patch state)
            (trap-station-session-camera/patch! state (edn/read-string id))
            (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn post [state data]
  (let [mr (m/->>=
            (either/right data)
            (api-util/transform-request resource-type :camelot.http.api.trap-station-session-camera.post/attributes)
            (validate-post state)
            (trap-station-session-camera/post! state)
            (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap #(api-util/created resource-base-uri %))
         (api-util/to-response))))

(defn get-with-id [state id]
  (let [mr (m/->>= (either/right id)
                   (api-util/transform-id)
                   (trap-station-session-camera/get-single state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn- get-all*
  [state trap-station-session-id]
  (either/right (trap-station-session-camera/get-all state trap-station-session-id)))

(defn get-all [state trap-station-session-id]
  (let [mr (m/->>= (trap-station-session/get-single state trap-station-session-id)
                   (map-not-found :trap-station-session-id)
                   (get-all* state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn delete [state id]
  (let [mr (trap-station-session-camera/mdelete! state (edn/read-string id))]
    (->> mr
         (m/fmap (constantly (hr/no-content)))
         (api-util/to-response))))
