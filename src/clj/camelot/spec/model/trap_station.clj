(ns camelot.spec.model.trap-station
  (:require [clojure.spec.alpha :as s]
            [clj-time.spec :as ts]))

(s/def ::trap-station-id int?)
(s/def ::survey-site-id int?)
(s/def ::trap-station-name string?)
(s/def ::trap-station-created ::ts/date-time)
(s/def ::trap-station-updated ::ts/date-time)
(s/def ::trap-station-longitude number?)
(s/def ::trap-station-latitude number?)
(s/def ::trap-station-notes (s/nilable string?))

(s/def ::trap-station
  (s/keys :req-un [::trap-station-id
                   ::survey-site-id
                   ::trap-station-name
                   ::trap-station-created
                   ::trap-station-updated
                   ::trap-station-longitude
                   ::trap-station-latitude]
          :opt-un [::trap-station-notes]))

(s/def ::ptrap-station
  (s/keys :opt-un [::trap-station-id
                   ::survey-site-id
                   ::trap-station-name
                   ::trap-station-created
                   ::trap-station-updated
                   ::trap-station-longitude
                   ::trap-station-latitude
                   ::trap-station-notes]))

(s/def ::ttrap-station
  (s/keys :req-un [::survey-site-id
                   ::trap-station-name
                   ::trap-station-longitude
                   ::trap-station-latitude]))
