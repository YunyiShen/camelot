(ns camelot.spec.model.trap-station-session
  (:require [clojure.spec.alpha :as s]
            [clj-time.spec :as ts]))

(s/def ::trap-station-session-id int?)
(s/def ::trap-station-id int?)
(s/def ::trap-station-session-name string?)
(s/def ::trap-station-session-created ::ts/date-time)
(s/def ::trap-station-session-updated ::ts/date-time)
(s/def ::trap-station-session-start-date ::ts/date-time)
(s/def ::trap-station-session-end-date (s/nilable ::ts/date-time))
(s/def ::trap-station-session-notes (s/nilable string?))
(s/def ::trap-station-session-label (s/nilable string?))

(s/def ::trap-station-session
  (s/keys :req-un [::trap-station-id
                   ::survey-site-id
                   ::trap-station-name
                   ::trap-station-created
                   ::trap-station-updated
                   ::trap-station-start-date]
          :opt-un [::trap-station-end-date
                   ::trap-station-notes]))

(s/def ::ptrap-station-session
  (s/keys :opt-un [::trap-station-session-id
                   ::trap-station-id
                   ::trap-station-start-date
                   ::trap-station-end-date
                   ::trap-station-notes]))

(s/def ::ttrap-station-session
  (s/keys :req-un [::trap-station-id
                   ::trap-station-start-date]
          :opt-un [::trap-station-end-date
                   ::trap-station-notes]))
