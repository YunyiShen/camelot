(ns camelot.services.species-search
  (:require
   [clj-http.client :as http]
   [cheshire.core :as json]
   [camelot.model.taxonomy :as taxonomy]
   [camelot.model.associated-taxonomy :as ataxonomy]))

(defn query-search
  "Search for a species by name"
  [_ {:keys [search]}]
  (let [r (http/get "http://api.catalogueoflife.org/dataset/3LR/nameusage/search"
                    {:query-params {"q" search
                                    "rank" "SPECIES"
                                    "status" "ACCEPTED"}})]
    (json/parse-string (:body r))))

(defn id-lookup
  "Look up a species by ID."
  [{:keys [id]}]
  (let [r (http/get (format "http://api.catalogueoflife.org/dataset/2242/taxon/%s/info" id))]
    (json/parse-string (:body r))))

(defn common-name
  "Extract the species common name."
  [result default-name]
  (if-let [record (some->> (get-in result ["vernacularNames"])
                           (filter #(= (get % "language") "eng"))
                           first)]
    (get-in record ["name"])
    default-name))

(defn result->tassociated-taxonomy
  "Transform response into a tassociated-taxonomy"
  [survey-id result]
  (let [taxon (get-in result ["taxon"])
        species (get-in taxon ["name" "specificEpithet"])
        genus (get-in taxon ["name" "genus"])
        default-name (str genus " " species)]
    (ataxonomy/tassociated-taxonomy
     {:taxonomy-genus genus
      :taxonomy-species species
      :taxonomy-common-name (common-name result default-name)
      :survey-id survey-id})))

(defn get-taxonomy-for-id
  "Get the taxonomic information for a species by its ID"
  [id]
  (let [resp (id-lookup {:id id})]
    (if-let [code (get resp "code")]
      (throw (RuntimeException. (str "Species ID lookup failed with code %d: %s" code (get resp "message"))))
      resp)))

(defn create-taxonomy-with-id
  "Create a species from the given ID for a survey"
  [state survey-id id]
  (->> id
       get-taxonomy-for-id
       (result->tassociated-taxonomy survey-id)
       (ataxonomy/create! state)))

(defn get-or-create-species
  "Get or create species"
  [state survey-id details]
  (let [ttax (taxonomy/ttaxonomy {:taxonomy-species (:species details)
                                  :taxonomy-genus (:genus details)})
        ts (taxonomy/get-specific-by-taxonomy state ttax)]
    (if ts
      (ataxonomy/ensure-associated state survey-id (:taxonomy-id ts))
      (create-taxonomy-with-id state survey-id (:id details)))))

(defn ensure-survey-species-known
  "Ensure all species have records"
  [state species survey-id]
  (map (partial get-or-create-species state survey-id) species))
