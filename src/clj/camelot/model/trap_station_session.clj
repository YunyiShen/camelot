(ns camelot.model.trap-station-session
  (:require
   [schema.core :as sch]
   [camelot.util.db :as db]
   [clj-time.format :as tf]
   [clojure.spec.alpha :as s]
   [camelot.spec.error :as error-spec]
   [camelot.spec.system :as sysspec]
   [camelot.spec.model.trap-station-session :as trap-station-session-spec]
   [camelot.spec.schema.state :refer [State]]
   [cats.monad.either :as either]
   [camelot.spec.cats :as cats-spec]
   [clj-time.core :as t]
   [camelot.model.media :as media]
   [camelot.model.camera :as camera]
   [camelot.translation.core :as tr]))

(def query (db/with-db-keys :trap-station-sessions))

(sch/defrecord TTrapStationSession
    [trap-station-id :- sch/Int
     trap-station-session-start-date :- org.joda.time.DateTime
     trap-station-session-end-date :- (sch/maybe org.joda.time.DateTime)
     trap-station-session-notes :- (sch/maybe sch/Str)]
  {sch/Any sch/Any})

(sch/defrecord TrapStationSession
    [trap-station-session-id :- sch/Int
     trap-station-session-created :- org.joda.time.DateTime
     trap-station-session-updated :- org.joda.time.DateTime
     trap-station-id :- sch/Int
     trap-station-session-start-date :- org.joda.time.DateTime
     trap-station-session-end-date :- (sch/maybe org.joda.time.DateTime)
     trap-station-session-notes :- (sch/maybe sch/Str)
     trap-station-session-label :- (sch/maybe sch/Str)]
  {sch/Any sch/Any})

(def trap-station-session map->TrapStationSession)
(def ttrap-station-session map->TTrapStationSession)

(def date-formatter (tf/formatter "yyyy-MM-dd"))

(defn- build-label
  [state start end]
  (let [sp (tf/unparse date-formatter start)]
    (if end
      (let [ep (tf/unparse date-formatter end)]
        (tr/translate state ::trap-station-session-closed-label sp ep))
      (tr/translate state ::trap-station-session-ongoing-label sp))))

(defn- add-label
  "Assoc a key for the label, which is a computed value."
  [state rec]
  (assoc rec :trap-station-session-label
         (build-label state
                      (:trap-station-session-start-date rec)
                      (:trap-station-session-end-date rec))))

(sch/defn get-all :- [TrapStationSession]
  [state :- State
   id :- sch/Int]
  (->> {:trap-station-id id}
       (query state :get-all)
       (map #(add-label state %))
       (map trap-station-session)))

(sch/defn get-all* :- [TrapStationSession]
  [state :- State]
  (->> (query state :get-all* {})
       (map #(add-label state %))
       (map trap-station-session)))

(defn get-specific
  [state id]
  (some->> {:trap-station-session-id id}
           (query state :get-specific)
           (first)
           (add-label state)
           (trap-station-session)))

(defn get-active
  "Return cameras which are active over the time range of the session with the given id."
  ([state session-id]
   (let [session (get-specific state session-id)]
     (when session
       (map :camera-id (query state :get-active session)))))
  ([state session-id session-camera-id]
   (let [session (get-specific state session-id)]
     (when session
       (->> session
            (query state :get-active)
            (remove #(= (:trap-station-session-camera-id %) session-camera-id))
            (map :camera-id))))))

(sch/defn get-specific-by-dates :- (sch/maybe TrapStationSession)
  [state :- State
   data :- TTrapStationSession]
  (some->> data
           (query state :get-specific-by-dates)
           (first)
           (add-label state)
           (trap-station-session)))

(sch/defn get-specific-by-trap-station-session-camera-id :- (sch/maybe TrapStationSession)
  [state :- State
   id :- sch/Int]
  (some->> {:trap-station-session-camera-id id}
           (query state :get-specific-by-trap-station-session-camera-id)
           (first)
           (trap-station-session)))

(defn- start-date-before-end-date?
  [data]
  (let [start (:trap-station-session-start-date data)
        end(:trap-station-session-end-date data)]
    (or (nil? end) (= start end) (t/before? start end))))

(sch/defn create! :- TrapStationSession
  [state :- State
   data :- TTrapStationSession]
  {:pre [(start-date-before-end-date? data)]}
  (let [record (query state :create<! data)]
    (trap-station-session (get-specific state (int (:1 record))))))

(defn update!
  "Update the value, dissoc'ing the label, as it's a computed field."
  [state id data]
  {:pre [(start-date-before-end-date? data)]}
  (db/with-transaction [s state]
    (let [data (dissoc data :trap-station-session-label)]
      (query s :update! (merge data {:trap-station-session-id id}))
      (get-specific s id))))

(defn- get-active-cameras
  [state params]
  (->> params
       (query state :get-active-cameras)
       (map :camera-id)
       (remove nil?)))

(defn delete!
  [state id]
  (when (get-specific state id)
    (let [fs (media/get-all-files-by-trap-station-session state id)
          ps {:trap-station-session-id id}
          cams (get-active-cameras state ps)]
      (query state :delete! ps)
      (media/delete-files! state fs)
      (camera/make-available state cams))
    id))

(sch/defn set-session-end-date!
  [state :- State
   data]
  (query state :set-session-end-date! data))

(defn get-or-create!
  [state data]
  (or (get-specific-by-dates state data)
      (create! state data)))

(s/fdef get-or-create!
  :args (s/cat :state ::sysspec/state :data ::trap-station-session-spec/ttrap-station-session)
  :ret ::trap-station-session-spec/trap-station-session)

(defn patch!
  "Update properties of a trap station session."
  [state id data]
  (if-let [entity (get-specific state id)]
    (either/right (->> data
                       (merge entity)
                       (update! state id)))
    (either/left {:error/type :error.type/not-found})))

(s/fdef patch!
        :args (s/cat :state ::sysspec/state
                     :id int?
                     :data ::trap-station-session-spec/ptrap-station-session)
        :ret (cats-spec/either? ::error-spec/error
                                ::trap-station-session-spec/site))

(defn mdelete!
  [state id]
  (if (delete! state id)
    (either/right id)
    (either/left {:error/type :error.type/not-found})))

(s/fdef mdelete!
        :args (s/cat :state ::sysspec/state
                     :id int?)
        :ret (cats-spec/either? ::error-spec/error
                                int?))

(defn post!
  [state data]
  (either/right (create! state (ttrap-station-session data))))

(s/fdef post!
        :args (s/cat :state ::sysspec/state
                     :data ::trap-station-session-spec/ttrap-station-session)
        :ret (cats-spec/either? ::error-spec/error
                                ::trap-station-session-spec/trap-station-session))

(defn get-single
  [state id]
  (if-let [s (get-specific state id)]
    (either/right s)
    (either/left {:error/type :error.type/not-found})))

(s/fdef get-single
        :args (s/cat :state ::sysspec/state :id int?)
        :ret (cats-spec/either? ::error-spec/error
                                ::trap-station-session-spec/trap-station-session))
